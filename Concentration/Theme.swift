//
//  Theme.swift
//  Concentration
//
//  Created by paul on 08/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation

enum Theme: String, CaseIterable {
    case animals = "🐧🐥🙉🦉🐼🐶🦁🐮🐷🐰"
    case aqueous = "🐋🦈🐠🐟🐬🐳🐡🦈🐙🦑"
    case sports = "🏀🏈⚾️🥌🛹🏏⛸🏂🥋🥊"
    case transports = "🚗🚕🚙🚌🚚🚜🚃✈️🚀🛳"
    case emoji = "😀😂😇🥰🤓😎🥳😩🥺🥶"
    case zodiac = "♈️♉️♊️♋️♌️♍️♎️♏️♐️♑️♒️♓️"
}
