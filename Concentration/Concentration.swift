//
//  Concentration.swift
//  Concentration
//
//  Created by paul on 05/12/2019.
//  Copyright © 2019 Stanford University. All rights reserved.
//

import Foundation

struct Concentration
{
    private(set) var cards = [Card]()
    
    private(set) var score = 0
    
    private(set) var flipCount = 0

    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            return cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly
        }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    mutating func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "\(#function) parameter:\(index): chosen index not in the cards")
        
        flipCount += 1
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                if cards[matchIndex] == cards[index] {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    
                    score += 2
                } else {
                    updateScore(for: cards[matchIndex])
                    updateScore(for: cards[index])
                }
                cards[index].isFaceUp = true

                cards[matchIndex].wasSeen = true
                cards[index].wasSeen = true
            } else {
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    private mutating func updateScore(for card: Card) {
        if card.wasSeen {
            score -= 1
        }
    }
    
    mutating func reset() {
        self = Concentration(numberOfPairsOfCards: self.cards.count / 2)
    }
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "\(#function) parameter:\(numberOfPairsOfCards): you must have at least one pair of cards")
        for _ in 1...numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }

        cards.shuffle()
    }
}

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
