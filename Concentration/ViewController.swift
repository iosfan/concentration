//
//  ViewController.swift
//  Concentration
//
//  Created by paul on 04/12/2019.
//  Copyright © 2019 Stanford University. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController
{
    private lazy var game = Concentration(numberOfPairsOfCards: numberOfPairsOfCards)
    
    var numberOfPairsOfCards: Int { (cardButtons.count + 1) / 2 }
    
    private var theme = Theme.allCases.randomElement()!.rawValue
    
    private var emoji = [Card:String]()
    
    @IBOutlet private weak var flipCountLabel: UILabel! {
        didSet {
            updateText(for: flipCountLabel)
        }
    }
    
    @IBOutlet weak var scoreLabel: UILabel! {
        didSet {
            updateText(for: scoreLabel)
        }
    }

    @IBOutlet private var cardButtons: [UIButton]!

    @IBAction private func newGame(_ sender: UIButton) {
        emoji = [Card:String]()
        theme = Theme.allCases.randomElement()!.rawValue

        game.reset()
        updateViewFromModel()
        updateLabels()
    }

    @IBAction private func touchCard(_ sender: UIButton) {
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.chooseCard(at: cardNumber)
            updateLabels()
            updateViewFromModel()
        } else {
            print("chosen card not in the cardButtons")
        }
    }
    
    private func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.665177896, blue: 0.07478602637, alpha: 0) : #colorLiteral(red: 1, green: 0.665177896, blue: 0.07478602637, alpha: 1)
            }
        }
    }
    
    private func updateLabels() {
        updateText(for: flipCountLabel)
        updateText(for: scoreLabel)
    }
    
    private func emoji(for card: Card) -> String {
        if emoji[card] == nil, theme.count > 0 {
            let randomStringIndex = theme.index(theme.startIndex, offsetBy: theme.count.arc4random)
            emoji[card] = String(theme.remove(at: randomStringIndex))
        }
        
        return emoji[card] ?? "?"
    }

    private func updateText(for label: UILabel) {
        let placeholder = (label == flipCountLabel) ? "Flips: \(game.flipCount)" : "Score: \(game.score)"
        let attributes: [NSAttributedString.Key:Any] = [
            .strokeWidth : 5.0,
            .strokeColor : #colorLiteral(red: 1, green: 0.6014475255, blue: 0.02449351689, alpha: 1)
        ]

        let attributedString = NSAttributedString(string: placeholder, attributes: attributes)
        label.attributedText = attributedString
    }
}

extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
