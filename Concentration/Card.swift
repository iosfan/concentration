//
//  Card.swift
//  Concentration
//
//  Created by paul on 05/12/2019.
//  Copyright © 2019 Stanford University. All rights reserved.
//

import Foundation

struct Card {
    var isFaceUp = false
    var isMatched = false
    var wasSeen = false
    private var identifier: String
    
    init() {
        self.identifier = UUID().uuidString
    }
}

extension Card : Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }

    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
}
